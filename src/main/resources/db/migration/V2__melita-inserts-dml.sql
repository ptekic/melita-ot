INSERT INTO user (id, username, password, roles,active)
VALUES (1, 'user', '$2a$10$CYAxJ6/SbVzStkbaZWE1EuyGpVNH5HiGCI16KrFhpGlSAupLExILm', 'ROLE_USER', true),
(2, 'admin', '$2a$10$CYAxJ6/SbVzStkbaZWE1EuyGpVNH5HiGCI16KrFhpGlSAupLExILm', 'ROLE_ADMIN', true);

INSERT INTO customer (id_customer, firstname, lastname, email, phone, install_date)
VALUES (1, 'John', 'Doe', 'doej@hotmail.com', '+38265232234', '2020-07-26'),
(2, 'Joanna', 'Doe', 'doejad@hotmail.com', '+382652552234', '2020-07-28');

INSERT INTO product (id_product, name)
VALUES (1, 'internet'), (2, 'cableTV'), (3, 'phone');

INSERT INTO pack(id, name, id_product_fk)
VALUES (1,'256',1), (2, '512', 1), (3, 'basic', 2), (4, 'premium', 2), (5, 'prepaid', 3), (6, 'postpaid', 3);

INSERT INTO ord (id, id_customer, status)
VALUES (1, 1, "ACCEPTED"), (2, 1, "CANCELLED");

INSERT INTO ord_pack (id, pack_id, ord_id)
VALUES (1,1,1), (2,3, 1),(3,5,1);