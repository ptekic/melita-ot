CREATE TABLE user
(
id int PRIMARY KEY,
username VARCHAR(255),
password VARCHAR(255),
roles VARCHAR(255),
active tinyint(1)
);

CREATE TABLE customer
(
id_customer int PRIMARY KEY AUTO_INCREMENT,
firstname VARCHAR(255),
lastname VARCHAR(255),
email VARCHAR(255),
phone VARCHAR(255),
install_date DATE
);

CREATE TABLE ord
(
id int PRIMARY KEY AUTO_INCREMENT,
id_customer int,
status VARCHAR(255),
FOREIGN KEY (id_customer) REFERENCES customer(id_customer)
);

CREATE TABLE product
(
id_product int PRIMARY KEY AUTO_INCREMENT,
name VARCHAR(255)
);

CREATE TABLE pack
(
id int PRIMARY KEY AUTO_INCREMENT,
name VARCHAR (255),
id_product_fk int,
FOREIGN KEY (id_product_fk) REFERENCES product(id_product)
);

CREATE TABLE ord_pack
(
id int PRIMARY KEY AUTO_INCREMENT,
pack_id int,
ord_id int,
FOREIGN KEY (pack_id) REFERENCES pack(id),
FOREIGN KEY (ord_id) REFERENCES ord(id)
);




