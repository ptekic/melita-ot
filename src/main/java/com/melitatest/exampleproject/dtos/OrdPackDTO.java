package com.melitatest.exampleproject.dtos;

import com.melitatest.exampleproject.model.OrdPack;

public class OrdPackDTO {
    Long id;
//    OrdDTO ordDTO;
    PackDTO packDTO;


    public OrdPackDTO(OrdPack ordPack) {
        this.id = ordPack.getId();
//        this.ordDTO = new OrdDTO(ordPack.getOrd());
        this.packDTO = new PackDTO(ordPack.getPack());
    }

    public OrdPackDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public OrdDTO getOrdDTO() {
//        return ordDTO;
//    }
//
//    public void setOrdDTO(OrdDTO ordDTO) {
//        this.ordDTO = ordDTO;
//    }

    public PackDTO getPackDTO() {
        return packDTO;
    }

    public void setPackDTO(PackDTO packDTO) {
        this.packDTO = packDTO;
    }

}
