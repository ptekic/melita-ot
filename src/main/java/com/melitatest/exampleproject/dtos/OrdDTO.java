package com.melitatest.exampleproject.dtos;

import com.melitatest.exampleproject.model.OrdPack;
import com.melitatest.exampleproject.model.Ord;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OrdDTO {

    Long id;
    String status;
    CustomerDTO customerDTO;
    List<PackDTO> packDTO;



    public OrdDTO(Ord order) {
        this.id = order.getId();
        this.status = order.getStatus();
        this.customerDTO = new CustomerDTO(order.getCustomer());
        this.packDTO = order.getPacks().stream().map(op->new PackDTO(op)).collect(Collectors.toList());
    }

    public OrdDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    private List<PackDTO> orderpacks(Ord order){
        List<PackDTO> packsList = new ArrayList<>();
        if(order != null){
            List<OrdPack> op = order.getOrdPacks();
        }
        return packsList;
    }

    public List<PackDTO> getPackDTO() {
        return packDTO;
    }

    public void setPackDTO(List<PackDTO> packDTO) {
        this.packDTO = packDTO;
    }
}
