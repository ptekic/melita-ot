package com.melitatest.exampleproject.dtos;

import com.melitatest.exampleproject.model.Product;

public class ProductDTO {


    String name;

    public ProductDTO(Product product) {
        this.name = product.getName();
    }

    public ProductDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
