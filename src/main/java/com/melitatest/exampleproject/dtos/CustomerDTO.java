package com.melitatest.exampleproject.dtos;

import com.melitatest.exampleproject.model.Customer;
import org.springframework.stereotype.Component;

import java.util.Date;

public class CustomerDTO {

    private String firstname;
    private String lastname;
    private String email;
    private String mobile;
    private String installationAddress;
    private Date installationDate;

    public CustomerDTO(Customer customer) {
        this.firstname = customer.getFirstname();
        this.lastname = customer.getLastname();
        this.email = customer.getEmail();
        this.mobile = customer.getPhone();
        this.installationDate = customer.getInstallDate();
    }

    public CustomerDTO() {
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getInstallationAddress() {
        return installationAddress;
    }

    public void setInstallationAddress(String installationAddress) {
        this.installationAddress = installationAddress;
    }

    public Date getInstallationDate() {
        return installationDate;
    }

    public void setInstallationDate(Date installationDate) {
        this.installationDate = installationDate;
    }

    public CustomerDTO customer2DTO(Customer customer) {
        CustomerDTO cdto = new CustomerDTO();
        if (customer != null) {
            cdto.firstname = customer.getFirstname();
            cdto.lastname = customer.getLastname();
            cdto.email = customer.getEmail();
            cdto.mobile = customer.getPhone();
            cdto.installationDate = customer.getInstallDate();
        }
        return cdto;
    }

}
