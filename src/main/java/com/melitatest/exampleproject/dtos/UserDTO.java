package com.melitatest.exampleproject.dtos;

import com.melitatest.exampleproject.model.User;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

public class UserDTO {

    Long id;
    String username;
    String password;
    String roles;

    public UserDTO(User user){
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.roles = user.getRoles();
    }
}
