package com.melitatest.exampleproject.dtos;
import com.melitatest.exampleproject.model.Pack;

public class PackDTO {

    private Long idPack;
    private String name;

    private ProductDTO productDTO;

    public PackDTO(Pack packs) {
        this.idPack = packs.getId();
        this.name = packs.getName();
        this.productDTO = new ProductDTO(packs.getProduct());
    }

    public PackDTO() {
    }

    public Long getIdPack() {
        return idPack;
    }

    public void setIdPack(Long idPack) {
        this.idPack = idPack;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }

}
