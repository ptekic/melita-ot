package com.melitatest.exampleproject.service;

public interface MailService {

    void sendEmail(String to, String bcc, String subject, String msgText);

}
