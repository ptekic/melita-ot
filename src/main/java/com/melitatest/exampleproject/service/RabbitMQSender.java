package com.melitatest.exampleproject.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class RabbitMQSender {

    private static final Logger LOGGER = Logger.getLogger(RabbitMQSender.class.getName());

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${melita.rabbitmq.exchange}")
    private String exchange;

    @Value("${melita.rabbitmq.routingkey}")
    private String routingkey;

    public void send(Object object) {
        rabbitTemplate.convertAndSend(exchange, routingkey, object);
        LOGGER.info("Send msg: " + object.toString());
    }

}
