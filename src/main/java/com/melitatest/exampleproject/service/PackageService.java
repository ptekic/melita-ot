package com.melitatest.exampleproject.service;

import com.melitatest.exampleproject.dtos.PackDTO;
import com.melitatest.exampleproject.model.Pack;

import java.util.List;

public interface PackageService {

    List<Pack> findAll();

    Pack findById(Long id);

    PackDTO save(Pack packages);

    boolean delete(String idPackage);

    PackDTO convert2DTO(Pack packages);

    Pack convert2Entity(PackDTO PackageDTO);
}
