package com.melitatest.exampleproject.service;

import com.melitatest.exampleproject.dtos.CustomerDTO;
import com.melitatest.exampleproject.model.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> findAll();

    Customer findById(Long id);

    CustomerDTO save(Customer customer);

    boolean delete(String idCustomer);

    CustomerDTO edit(CustomerDTO customerDTO);

    CustomerDTO convert2DTO(Customer customer);

    Customer convert2Entity(CustomerDTO customerDTO);

    boolean exist(CustomerDTO customerDTO);

}
