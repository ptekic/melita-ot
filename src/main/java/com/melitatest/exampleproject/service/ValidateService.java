package com.melitatest.exampleproject.service;

import com.melitatest.exampleproject.dtos.OrdDTO;

public interface ValidateService {

    public boolean validateOrder(OrdDTO order);

}
