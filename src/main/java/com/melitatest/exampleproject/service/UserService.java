package com.melitatest.exampleproject.service;

import com.melitatest.exampleproject.model.User;

public interface UserService {

    public User findByUsername(String username);
}
