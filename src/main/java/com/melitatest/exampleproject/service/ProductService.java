package com.melitatest.exampleproject.service;

import com.melitatest.exampleproject.dtos.ProductDTO;
import com.melitatest.exampleproject.model.Product;

import java.util.List;

public interface ProductService {

    List<Product> findAll();

    Product findById(Long id);

    ProductDTO save(Product product);

    boolean delete(String idProduct);

    ProductDTO convert2DTO(Product product);

    Product convert2Entity(ProductDTO productDTO);
}
