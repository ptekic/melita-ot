package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.dtos.OrdDTO;
import com.melitatest.exampleproject.model.Ord;
import com.melitatest.exampleproject.model.OrdPack;
import com.melitatest.exampleproject.model.Pack;
import com.melitatest.exampleproject.repository.CustomerRepository;
import com.melitatest.exampleproject.repository.OrdPackRepository;
import com.melitatest.exampleproject.repository.OrdRepository;
import com.melitatest.exampleproject.repository.PackRepository;
import com.melitatest.exampleproject.service.OrdService;
import com.melitatest.exampleproject.service.PackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrdServiceImpl implements OrdService {

    @Autowired
    OrdRepository ordRepository;

    @Autowired
    PackRepository packRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrdPackRepository ordPackRepository;

    @Autowired
    CustomerServiceImpl customerServiceImpl;

    @Autowired
    PackageService packageService;

    @Override
    public List<Ord> findAll(){
        return ordRepository.findAll();
    }

    @Override
    public Ord findById(Long id){
        return ordRepository.findById(id).get();
    }

    @Override
    public OrdDTO save(Ord order){
        return convert2DTO(ordRepository.save(order));
    }


    @Override
    public boolean delete(String idOrders){
        Ord order = ordRepository.findById(Long.parseLong(idOrders)).get();
        boolean deleted = false;
        if(order!=null){
            ordRepository.delete(order);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public OrdDTO convert2DTO(Ord order){
        OrdDTO cdto = new OrdDTO();
        if(order!=null) {
            cdto = new OrdDTO(order);
        }
        return cdto;
    }

    @Override
    public Ord convert2Entity(OrdDTO orderDTO){
        Ord order= new Ord();

        order.setCustomer(customerRepository.findByEmail(orderDTO.getCustomerDTO().getEmail()));
        List<Pack> packs = orderDTO.getPackDTO().stream().map(p->packRepository.findByName(p.getName())).collect(Collectors.toList());
        List<OrdPack> opl = new ArrayList<OrdPack>();
        for (Pack pack : packs) {
            OrdPack op = new OrdPack();
            op.setPack(pack);
            op.setOrd(order);
            opl.add(op);

        }
        order.setOrdPacks(opl);
        return order;
    }
}
