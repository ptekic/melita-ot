package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.model.User;
import com.melitatest.exampleproject.repository.UserRepository;
import com.melitatest.exampleproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).get();
    }

}
