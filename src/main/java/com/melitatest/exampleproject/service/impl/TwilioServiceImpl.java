package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.service.TwilioService;
import org.springframework.stereotype.Service;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

import java.net.URI;
import java.util.logging.Logger;


@Service
public class TwilioServiceImpl implements TwilioService {

    private static final Logger LOGGER = Logger.getLogger(TwilioServiceImpl.class.getName());
    private static final String SMS = "Your order is received. Melita order tracking.";

    @Override
    public void sendSms(String phone) {
        Message message = Message.creator(new PhoneNumber(phone), new PhoneNumber(phone), SMS).create();
        LOGGER.info("SMS sent to: ");
    }

    @Override
    public void call(String phone) {
        Call call = Call.creator(new PhoneNumber(phone), new PhoneNumber(phone), URI.create("http://demo.twilio.com/docs/voice.xml")).create();
        LOGGER.info("call::" + call.getTo());
    }

}
