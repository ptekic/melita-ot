package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.dtos.CustomerDTO;
import com.melitatest.exampleproject.model.Customer;
import com.melitatest.exampleproject.repository.CustomerRepository;
import com.melitatest.exampleproject.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public List<Customer> findAll(){
        return customerRepository.findAll();
    }

    @Override
    public Customer findById(Long id){
        return customerRepository.findById(id).get();
    }

    @Override
    public CustomerDTO save(Customer customer){
        return convert2DTO(customerRepository.save(customer));
    }

    @Override
    public CustomerDTO edit(CustomerDTO customerDTO){

        Customer editCustomer = customerRepository.findByEmail(customerDTO.getEmail());
        editCustomer.setFirstName(customerDTO.getFirstname());
        editCustomer.setLastname(customerDTO.getLastname());
        editCustomer.setPhone(customerDTO.getMobile());
        editCustomer.setInstallDate(customerDTO.getInstallationDate());


        return convert2DTO(customerRepository.save(editCustomer));
    }

    @Override
    public boolean delete(String idCustomer){
        Customer customer = customerRepository.findById(Long.parseLong(idCustomer)).get();
        boolean deleted = false;
        if (customer!=null){
            customerRepository.delete(customer);
            deleted = false;
        }

        return deleted;
    }

    @Override
    public CustomerDTO convert2DTO(Customer customer){
        CustomerDTO cdto = new CustomerDTO();
        if(customer!=null) {
            cdto = new CustomerDTO(customer);
        }
        return cdto;
    }

    @Override
    public Customer convert2Entity(CustomerDTO customerDTO){
        Customer customer= new Customer();
        customer.setFirstName(customerDTO.getFirstname());
        customer.setLastname(customerDTO.getLastname());
        customer.setEmail(customerDTO.getEmail());
        customer.setPhone(customerDTO.getMobile());
        customer.setInstallDate(customerDTO.getInstallationDate());
        return customer;
    }

    @Override
    public boolean exist(CustomerDTO customerDTO){
        return customerRepository.findByEmail(customerDTO.getEmail()) !=null;
    }



}
