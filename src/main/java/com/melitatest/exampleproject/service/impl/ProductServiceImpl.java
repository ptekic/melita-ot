package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.dtos.ProductDTO;
import com.melitatest.exampleproject.model.Product;
import com.melitatest.exampleproject.repository.ProductRepository;
import com.melitatest.exampleproject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> findAll() {
        return productRepository.findAll();
    }

    @Override
    public Product findById(Long id) {
        return productRepository.findById(id).get();
    }

    @Override
    public ProductDTO save(Product product) {
        return convert2DTO(productRepository.save(product));
    }


    @Override
    public boolean delete(String idProduct) {
        Product product = productRepository.findById(Long.parseLong(idProduct)).get();
        boolean deleted = false;
        if (product != null) {
            productRepository.delete(product);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public ProductDTO convert2DTO(Product product) {
        ProductDTO cdto = new ProductDTO();
        if (product != null) {
            cdto = new ProductDTO(product);
        }
        return cdto;
    }

    @Override
    public Product convert2Entity(ProductDTO productDTO) {
        Product product;
        product = new Product();
        product.setName(productDTO.getName());
        return product;
    }
}
