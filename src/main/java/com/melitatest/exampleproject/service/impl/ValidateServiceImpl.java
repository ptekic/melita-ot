package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.dtos.OrdDTO;
import com.melitatest.exampleproject.model.Ord;
import com.melitatest.exampleproject.service.ValidateService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Service
public class ValidateServiceImpl implements ValidateService {

    private static final Logger LOGGER = Logger.getLogger(MailServiceImpl.class.getName());
    private static final String URL_ENDPOINT  = "http://localhost:8090/api/v1/validate";

    @Override
    public boolean validateOrder(OrdDTO order){
        boolean valid = false;
        if(order!=null){
            RestTemplate restTemplate = new RestTemplate();
            Map<String, String> reqOrd = new HashMap<>();
            reqOrd.put("status", order.getStatus());
            reqOrd.put("customerName",order.getCustomerDTO().getEmail());
            reqOrd.put("packName", "Internet");


            ResponseEntity<Void> response = restTemplate.postForEntity(URL_ENDPOINT, reqOrd, Void.class);

            if (response.getStatusCode() == HttpStatus.OK) {
                LOGGER.info("Valid order");
                valid = true;
            } else {
                LOGGER.info("Invalid order");
            }

        }
        return valid;
    }
}
