package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class MailServiceImpl implements MailService {

    private static final Logger LOGGER = Logger.getLogger(MailServiceImpl.class.getName());
    private static final String USER_SUBJECT = "melita order tracking";
    private static final String DOC_SUBJECT = "new order arrived to server";
    private static final String ACTIVATE_MSG = "Please activate your account for eDoc server \n  username: ";
    private static final String MAIL_BCC = "melita.test.mail@gmail.com";

    @Autowired
    JavaMailSender javaMailSender;

    /**
     *
     * @param to
     * @param subject
     * @param msgText
     */
    @Override
    public void sendEmail(String to, String bcc, String subject, String msgText) {

        LOGGER.info("Sending Email to " + to);
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(to.trim());
        msg.setBcc(bcc.trim());

        msg.setSubject(subject);
        msg.setText(msgText);

        javaMailSender.send(msg);
        LOGGER.info("Email sent to: {}" + to);

    }

}
