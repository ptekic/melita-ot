package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.OtAppApplication;
import com.melitatest.exampleproject.dtos.PackDTO;
import com.melitatest.exampleproject.model.Pack;
import com.melitatest.exampleproject.model.Product;
import com.melitatest.exampleproject.repository.PackRepository;
import com.melitatest.exampleproject.repository.ProductRepository;
import com.melitatest.exampleproject.service.PackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class PackageServiceImpl implements PackageService {

    private static final Logger LOGGER = Logger.getLogger(PackageService.class.getName());

    @Autowired
    PackRepository packRepository;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductServiceImpl productServiceImpl;

    @Override
    public List<Pack> findAll(){
        return packRepository.findAll();
    }

    @Override
    public Pack findById(Long id){
        return packRepository.findById(id).get();
    }

    @Override
    public PackDTO save(Pack packages){
        return convert2DTO(packRepository.save(packages));
    }


    @Override
    public boolean delete(String idPackage){
        Pack packs = packRepository.findById(Long.parseLong(idPackage)).get();
        boolean deleted = false;
        if(packs!=null){
            packRepository.delete(packs);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public PackDTO convert2DTO(Pack packs){
        PackDTO cdto = new PackDTO();
        if(packs!=null) {
            cdto = new PackDTO(packs);
        }
        return cdto;
    }

    @Override
    public Pack convert2Entity(PackDTO packDTO){
        Pack pack= new Pack();
        Product prod = productRepository.findByName(packDTO.getProductDTO().getName());
        if (prod != null ){
            pack.setName(packDTO.getName());
            pack.setProduct(prod);
        }
        return pack;
    }
}
