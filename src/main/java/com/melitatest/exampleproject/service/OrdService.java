package com.melitatest.exampleproject.service;

import com.melitatest.exampleproject.dtos.OrdDTO;
import com.melitatest.exampleproject.model.Ord;

import java.util.List;

public interface OrdService {

    List<Ord> findAll();

    Ord findById(Long id);

    OrdDTO save(Ord ord);

    boolean delete(String idOrders);

    OrdDTO convert2DTO(Ord ord);

    Ord convert2Entity(OrdDTO ordDTO);

}
