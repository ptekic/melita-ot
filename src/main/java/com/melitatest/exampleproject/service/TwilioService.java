package com.melitatest.exampleproject.service;

public interface TwilioService {

    void sendSms(String phone);

    void call(String phone);

}
