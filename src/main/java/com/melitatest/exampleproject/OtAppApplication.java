package com.melitatest.exampleproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;

@SpringBootApplication
public class OtAppApplication {
    private static final Logger LOGGER = Logger.getLogger(OtAppApplication.class.getName());

    public static void main(String[] args) {
        SpringApplication.run(OtAppApplication.class, args);
        LOGGER.info("STARTED");
    }

}
