package com.melitatest.exampleproject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.ApiKeyVehicle;
import springfox.documentation.swagger.web.SecurityConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
public class SwaggerConfig {

//    @Value("${rest.security.issuer-uri}")
//    private String AUTH_SERVER;
//
    @Value("${swaggerurl}")
    private String AUTHORIZE;

    @Value("${swaggerurl}")
    private String TOKEN;

    //    @Value("${security.oauth2.client.client-id}")
//    private String CLIENT_ID = "CLIENT_ID";//AuthorizationServerConfig.CLIENT_ID;

    //    @Value("${security.oauth2.client.client-secret}")
//    private String CLIENT_SECRET = "navigator";//AuthorizationServerConfig.CLIENT_SECRET;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.melitatest.exampleproject.api"))
                .paths(PathSelectors.regex("/api/.*"))
                .build()
//				.securitySchemes(Arrays.asList(securityScheme()))
//                .securityContexts(Arrays.asList(securityContext()))
//                .securitySchemes(Collections.singletonList(securitySchema()))
//                .securityContexts(Collections.singletonList(securityContext()))
                .apiInfo(metaData());
    }

    private ApiInfo metaData() {
        List<VendorExtension> vendorExtensions = new ArrayList<>();
        ApiInfo apiInfo = new ApiInfo(
                "Melita sample project",
                "Melita sample project API",
                "1.0",
                "Terms of service",
                new Contact("Melita Ltd.", "https://www.melita.com/", "office@melita.com"),
                "Melita Ltd.",
                "https://www.melita.com/", vendorExtensions);

        return apiInfo;

    }

//	@Bean
//    public SecurityConfiguration security() {
//        return SecurityConfigurationBuilder.builder()
//            .clientId(CLIENT_ID)
//            .clientSecret(CLIENT_SECRET)
//            .scopeSeparator(" ")
//            .useBasicAuthenticationWithAccessCodeGrant(true)
//            .build();
//    }

//    @Bean
//    public SecurityConfiguration security() {
//        return new SecurityConfiguration(CLIENT_ID, CLIENT_SECRET, "", "", "Bearer access_token", ApiKeyVehicle.HEADER, "Authorization",",");
//    }

//    private SecurityScheme securityScheme() {
//        GrantType grantType = new AuthorizationCodeGrantBuilder()
//            .tokenEndpoint(new TokenEndpoint(TOKEN, "oauthtoken"))
//            .tokenRequestEndpoint(
//              new TokenRequestEndpoint(AUTHORIZE, CLIENT_ID, CLIENT_SECRET))
//            .build();
//
//        SecurityScheme oauth = new OAuthBuilder().name("spring_oauth")
//            .grantTypes(Arrays.asList(grantType))
//            .scopes(Arrays.asList(scopes()))
//            .build();
//        return oauth;
//    }
//
//    private AuthorizationScope[] scopes() {
//        AuthorizationScope[] scopes = {
//          new AuthorizationScope("openid", "openid") };
//        return scopes;
//    }

    private AuthorizationScope[] scopes() {
        AuthorizationScope[] scopes = {
                new AuthorizationScope("read", "read all"), new AuthorizationScope("trust", "trust all") , new AuthorizationScope("write", "access all")  };
        return scopes;
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder()
                .securityReferences(
                        Arrays.asList(new SecurityReference("spring_oauth", scopes())))
                .forPaths(PathSelectors.regex("/api/.*"))
                .build();
    }

    private OAuth securitySchema() {

        List<AuthorizationScope> authorizationScopeList = new ArrayList();
        authorizationScopeList.add(new AuthorizationScope("read", "read all"));
        authorizationScopeList.add(new AuthorizationScope("trust", "trust all"));
        authorizationScopeList.add(new AuthorizationScope("write", "access all"));

        List<GrantType> grantTypes = new ArrayList();
        GrantType creGrant = new ResourceOwnerPasswordCredentialsGrant(AUTHORIZE);

        grantTypes.add(creGrant);

        return new OAuth("spring_oauth", authorizationScopeList, grantTypes);

    }

    private List<SecurityReference> defaultAuth() {

        final AuthorizationScope[] authorizationScopes = new AuthorizationScope[3];
        authorizationScopes[0] = new AuthorizationScope("read", "read all");
        authorizationScopes[1] = new AuthorizationScope("trust", "trust all");
        authorizationScopes[2] = new AuthorizationScope("write", "write all");

        return Collections.singletonList(new SecurityReference("spring_oauth", authorizationScopes));
    }
}
