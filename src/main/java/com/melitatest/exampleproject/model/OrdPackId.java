package com.melitatest.exampleproject.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class OrdPackId implements Serializable {

    @Column(name = "pack_id")
    private Long packId;

    @Column(name = "ord_id")
    private Long ordId;

    private OrdPackId() {}

    public OrdPackId(
            Long packId,
            Long ordId) {
        this.packId = packId;
        this.ordId = ordId;
    }

    public Long getPackId() {
        return packId;
    }

    public void setPackId(Long packId) {
        this.packId = packId;
    }

    public Long getOrdId() {
        return ordId;
    }

    public void setOrdId(Long ordId) {
        this.ordId = ordId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdPackId ordPackId = (OrdPackId) o;
        return Objects.equals(packId, ordPackId.packId) &&
                Objects.equals(ordId, ordPackId.ordId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(packId, ordId);
    }

}
