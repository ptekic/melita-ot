package com.melitatest.exampleproject.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity(name="ord")
@Table(name="ord")
public class Ord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String status;

    @OneToOne
    @JoinColumn(name = "id_customer")
    Customer customer;

    @JsonIgnore
    @OneToMany(mappedBy = "ord",  cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<OrdPack> ordPacks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public List<OrdPack> getOrdPacks() {
        return ordPacks;
    }

    public List<Pack> getPacks() {
        return ordPacks.stream().map(op->op.getPack()).collect(Collectors.toList());
    }

    public void setOrdPacks(List<OrdPack> ordPacks) {
        this.ordPacks = ordPacks;
    }

}
