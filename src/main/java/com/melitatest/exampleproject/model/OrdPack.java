package com.melitatest.exampleproject.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "ord_pack")
public class OrdPack implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

//    @EmbeddedId
//    private OrdPackId id;

    @ManyToOne
    @JoinColumn(name = "ord_id")
    private Ord ord;

    @ManyToOne
    @JoinColumn(name = "pack_id")
    private Pack pack;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Ord getOrd() {
        return ord;
    }

    public void setOrd(Ord ord) {
        this.ord = ord;
    }

    public Pack getPack() {
        return pack;
    }

    public void setPack(Pack pack) {
        this.pack = pack;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdPack ordPack = (OrdPack) o;
        return Objects.equals(id, ordPack.id) &&
                Objects.equals(id, ordPack.id) &&
                Objects.equals(pack, ordPack.pack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, id, pack);
    }

}
