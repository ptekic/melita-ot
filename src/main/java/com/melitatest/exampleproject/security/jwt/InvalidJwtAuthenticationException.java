package com.melitatest.exampleproject.security.jwt;

import org.springframework.security.core.AuthenticationException;

public class InvalidJwtAuthenticationException extends AuthenticationException {
    private static final long serialVersionUID = 7599588750003303123L;

    public InvalidJwtAuthenticationException(String e) {
        super(e);
    }
}
