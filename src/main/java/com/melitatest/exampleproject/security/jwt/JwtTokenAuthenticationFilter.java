package com.melitatest.exampleproject.security.jwt;

import com.melitatest.exampleproject.api.CustomerController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

public class JwtTokenAuthenticationFilter extends GenericFilterBean {

    private static final Logger LOGGER = Logger.getLogger(JwtTokenAuthenticationFilter.class.getName());

    private JwtTokenProvider jwtTokenProvider;

    public JwtTokenAuthenticationFilter(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws IOException, ServletException {

        String token = jwtTokenProvider.resolveToken((HttpServletRequest) req);
        if (token != null && jwtTokenProvider.validateToken(token)) {
            Authentication auth = jwtTokenProvider.getAuthentication(token);

//            LOGGER.log(DEBUG,"");
//            LOGGER.debug(auth.getName());
//            LOGGER.debug(auth.getPrincipal().toString());
//            LOGGER.debug(auth.getAuthorities().toString());
//            LOGGER.debug(auth.isAuthenticated() + "");

            if (auth != null) {
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        }
        filterChain.doFilter(req, res);
    }

}
