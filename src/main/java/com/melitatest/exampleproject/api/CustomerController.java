package com.melitatest.exampleproject.api;

import com.melitatest.exampleproject.dtos.CustomerDTO;
import com.melitatest.exampleproject.service.CustomerService;
import com.melitatest.exampleproject.service.MailService;
import com.melitatest.exampleproject.service.RabbitMQSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {

    private static final Logger LOGGER = Logger.getLogger(CustomerController.class.getName());

    @Autowired
    private CustomerService customerService;

    @Autowired
    MailService mailService;

    @Autowired
    RabbitMQSender rabbitMQSender;

    @GetMapping()
    public ResponseEntity<List<CustomerDTO>> getAll() {

        List<CustomerDTO> customerList = customerService.findAll()
                .stream()
                .map(c -> customerService.convert2DTO(c))
                .collect(Collectors.toList());

        return new ResponseEntity<List<CustomerDTO>>(customerList, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDTO> getById(@PathVariable Long id) {
        LOGGER.info("Get customer with id: " + id);
        return (customerService.convert2DTO(customerService.findById(id)) != null) ? new ResponseEntity<CustomerDTO>(customerService.convert2DTO(customerService.findById(id)), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<CustomerDTO> saveCustomer(@RequestBody(required = true) CustomerDTO customerDTO) {
        CustomerDTO cd = customerService.save(customerService.convert2Entity(customerDTO));
        return (cd != null) ? new ResponseEntity<>(customerDTO, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCustomer(@PathVariable String id){
        return customerService.delete(id)?ResponseEntity.ok(id):ResponseEntity.notFound().build();
    }

    @PutMapping()
    public ResponseEntity<CustomerDTO> editCustomer(@RequestBody(required = true) CustomerDTO customerDTO){
        CustomerDTO cd = customerService.edit(customerDTO);
        return (cd != null) ? new ResponseEntity<>(customerDTO, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
