package com.melitatest.exampleproject.api;

import com.melitatest.exampleproject.dtos.OrdDTO;
import com.melitatest.exampleproject.model.Ord;
import com.melitatest.exampleproject.model.Pack;
import com.melitatest.exampleproject.service.OrdService;
import com.melitatest.exampleproject.service.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/orders")
public class OrdersController {
    private static final Logger LOGGER = Logger.getLogger(OrdersController.class.getName());

    @Autowired
    private OrdService orderService;

    @Autowired
    private ValidateService validateService;

    @GetMapping()
    public ResponseEntity<List<OrdDTO>> getAll() {

        List<OrdDTO> orderList = orderService.findAll()
                .stream()
                .map(c -> orderService.convert2DTO(c))
                .collect(Collectors.toList());

        return new ResponseEntity<List<OrdDTO>>(orderList, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<OrdDTO> getById(@PathVariable Long id) {
        LOGGER.info("Get order with id: " + id);
        return (orderService.convert2DTO(orderService.findById(id)) != null) ? new ResponseEntity<OrdDTO>(orderService.convert2DTO(orderService.findById(id)), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<OrdDTO> saveOrders(@RequestBody(required = true) OrdDTO orderDTO) {
        OrdDTO cd = orderService.save(orderService.convert2Entity(orderDTO));
//        LOGGER.info("Send mail to tablet.drls@gmail.com ");
//        mailService.sendEmail("tablet.drls@gmail.com","melita.test.mail@gmail.com","melita test mail sbjct", "melita test mail body");
//        LOGGER.info("Send to rabbitmq message ");
//        rabbitMQSender.send("Test msg");
        orderDTO.setStatus("ACCEPTED");
        validateService.validateOrder(orderDTO);
        return (cd != null) ? new ResponseEntity<>(orderDTO, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteOrders(@PathVariable String id){
        return orderService.delete(id)?ResponseEntity.ok(id):ResponseEntity.notFound().build();
    }
}
