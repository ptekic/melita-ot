package com.melitatest.exampleproject.api;

import com.melitatest.exampleproject.dtos.ProductDTO;
import com.melitatest.exampleproject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    private static final Logger LOGGER = Logger.getLogger(ProductController.class.getName());

    @Autowired
    private ProductService productService;

    @GetMapping()
    public ResponseEntity<List<ProductDTO>> getAll() {

        List<ProductDTO> productList = productService.findAll()
                .stream()
                .map(c -> productService.convert2DTO(c))
                .collect(Collectors.toList());

        return new ResponseEntity<List<ProductDTO>>(productList, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductDTO> getById(@PathVariable Long id) {
        LOGGER.info("Get product with id: " + id);
        return (productService.convert2DTO(productService.findById(id)) != null) ? new ResponseEntity<ProductDTO>(productService.convert2DTO(productService.findById(id)), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<ProductDTO> saveProduct(@RequestBody(required = true) ProductDTO productDTO) {
        ProductDTO cd = productService.save(productService.convert2Entity(productDTO));
        return (cd != null) ? new ResponseEntity<>(productDTO, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        return productService.delete(id)?ResponseEntity.ok(id):ResponseEntity.notFound().build();
    }
}
