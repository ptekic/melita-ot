package com.melitatest.exampleproject.api;

import com.melitatest.exampleproject.dtos.PackDTO;
import com.melitatest.exampleproject.repository.PackRepository;
import com.melitatest.exampleproject.service.PackageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.melitatest.exampleproject.model.Pack;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
@RestController
@RequestMapping("/api/v1/packages")
public class PackController {
    private static final Logger LOGGER = Logger.getLogger(PackController.class.getName());

    @Autowired
    private PackageService packageService;

    @Autowired
    private PackRepository packRepository;

    @GetMapping()
    public ResponseEntity<List<PackDTO>> getAll() {

        List<PackDTO> packageList = packageService.findAll()
                .stream()
                .map(c -> packageService.convert2DTO(c))
                .collect(Collectors.toList());

        return new ResponseEntity<List<PackDTO>>(packageList, HttpStatus.OK);

    }

    @GetMapping("/entity")
    public ResponseEntity<List<Pack>> getAllP() {

        List<Pack> packs = packRepository.findAll();

        return new ResponseEntity<List<Pack>>(packs, HttpStatus.OK);

    }

    @GetMapping("/{id}")
    public ResponseEntity<PackDTO> getById(@PathVariable Long id) {
        LOGGER.info("Get package with id: " + id);
        return (packageService.convert2DTO(packageService.findById(id)) != null) ? new ResponseEntity<PackDTO>(packageService.convert2DTO(packageService.findById(id)), HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping()
    public ResponseEntity<PackDTO> savePackage(@RequestBody(required = true) PackDTO packageDTO) {
        PackDTO cd = packageService.save(packageService.convert2Entity(packageDTO));
        return (cd != null) ? new ResponseEntity<>(packageDTO, HttpStatus.CREATED) : new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePackage(@PathVariable String id){
        return packageService.delete(id)?ResponseEntity.ok(id):ResponseEntity.notFound().build();
    }
}
