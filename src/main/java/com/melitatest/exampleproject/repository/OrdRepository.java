package com.melitatest.exampleproject.repository;

import com.melitatest.exampleproject.model.Ord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdRepository extends JpaRepository<Ord, Long> {
}
