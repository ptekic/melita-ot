package com.melitatest.exampleproject.repository;

import com.melitatest.exampleproject.model.Pack;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PackRepository extends JpaRepository<Pack, Long> {
    public Pack findByName(String name);
}
