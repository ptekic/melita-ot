package com.melitatest.exampleproject.util;

public enum OrderStatus {
    ACCEPTED,
    CANCELLED,
    UPDATED
}
