package com.melitatest.exampleproject.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ProductTest {

    Product product;

    @BeforeEach
    public void setUp() {
        product = new Product();
    }

    @Test
    public void getId() throws Exception {
        //given
        Long id = 5L;

        //when
        product.setIdProduct(id);
        //then
        assertEquals(id, product.getIdProduct());
    }

    @Test
    public void getName() throws Exception {
        //given
        String name = "Internet";

        //when
        product.setName(name);;

        //then
        assertEquals(name, product.getName());

    }

}
