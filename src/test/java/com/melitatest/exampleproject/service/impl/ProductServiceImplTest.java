package com.melitatest.exampleproject.service.impl;

import com.melitatest.exampleproject.model.Product;
import com.melitatest.exampleproject.repository.ProductRepository;
import com.melitatest.exampleproject.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {

    ProductService productService;

    @Mock
    ProductRepository productRepository;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        productService = new ProductServiceImpl();
    }

    @Test
    void testGetAll() {
        //GIVEN
//        Product product = new Product();
        Integer numberInitilizedProducts = 3;
//        ArrayList<Product> productsData = new ArrayList<>();
//        productsData.add(product);

//        when(productRepository.findAll()).thenReturn(productsData);

        //WHEN
        List<Product> products = productService.findAll();

        //THEN
        assertEquals(products.size(), numberInitilizedProducts);

        verify(productRepository, times(1)).findAll();
    }

    @Test
    void testSave() {
        //GIVEN
        Product productData = new Product();
        Product productToSave = new Product();
        when(productRepository.save(productToSave)).thenReturn(productData);

        //WHEN
        Product product = productService.convert2Entity(productService.save(productToSave));

        //THEN
        assertEquals(product, productData);
        verify(productRepository, times(1)).save(productToSave);
    }

    @Test
    void testGetOne() {
        //GIVEN
        Long id = 1L;

        Product product = new Product();

        when(productRepository.findById(id).get()).thenReturn(product);

        //WHEN
        Product productFetched = productService.findById(id);

        //THEN
        assertEquals(product.getIdProduct(), productFetched.getIdProduct());
        assertEquals(product.getName(), productFetched.getName());

        verify(productRepository, times(1)).findById(id);
    }

}
