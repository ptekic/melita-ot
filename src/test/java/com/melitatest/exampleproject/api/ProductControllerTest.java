package com.melitatest.exampleproject.api;

import com.melitatest.exampleproject.dtos.ProductDTO;
import com.melitatest.exampleproject.model.Product;
import com.melitatest.exampleproject.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

public class ProductControllerTest {

    ProductController productController;

    @Mock
    ProductService productService;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        productController = new ProductController();
    }

    @Test
    void testGetAllProducts() {
        //GIVEN
//        ProductPackage productPackage = new ProductPackage(1L, "1 Gbit", 2L);
//        ArrayList<ProductPackage> productPackagesData = new ArrayList<>();
//        productPackagesData.add(productPackage);
        Product product = new Product();
        product.setIdProduct(9L);
        product.setName("ADSL");
        ArrayList<Product> productsData = new ArrayList<>();
        productsData.add(product);

        ArrayList<ProductDTO> productsDTOData = new ArrayList<>();
        productsData.forEach(p -> productsDTOData.add(new ProductDTO(p)));

        //WHEN
        when(productService.findAll()).thenReturn(productsData);

        assertNotEquals(entity.getBody(), productsDTOData);
        verify(productService, times(1)).findAll();

    }
}
